﻿using System;
using Restaurant.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dblayer;
using static System.Collections.Specialized.BitVector32;

namespace Restaurant.Controllers
{
   
    public class SettingController : Controller
    {
        Main_Restaurant_DbEntities Db = new Main_Restaurant_DbEntities();




        public ActionResult List_Genders(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }
            var gender = new CreateRetrieveUpdate_GenderMV(id);
            return View(gender);
        }

        [HttpPost]
        public ActionResult List_Genders(CreateRetrieveUpdate_GenderMV cRU_GenderMV)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                if (cRU_GenderMV.GenderID == 0)
                {
                    var checkexist = Db.GenderTables.Where(g => g.GenderTitle == cRU_GenderMV.GenderTitle).FirstOrDefault();
                    if (checkexist == null)
                    {
                        var gender = new GenderTable();
                        gender.GenderTitle = cRU_GenderMV.GenderTitle;
                        Db.GenderTables.Add(gender);
                        Db.SaveChanges();
                        return RedirectToAction("List_Genders", new { id = 0 });
                    }
                    else
                    {
                        ModelState.AddModelError("GenderTitle", "All Ready Exist!");
                    }
                }
                else
                {
                    var checkexist = Db.GenderTables.Where(g => g.GenderTitle == cRU_GenderMV.GenderTitle && g.GenderID != cRU_GenderMV.GenderID).FirstOrDefault();
                    if (checkexist == null)
                    {
                        var editgender = Db.GenderTables.Find(cRU_GenderMV.GenderID);
                        editgender.GenderTitle = cRU_GenderMV.GenderTitle;
                        Db.Entry(editgender).State = System.Data.Entity.EntityState.Modified;
                        Db.SaveChanges();
                        return RedirectToAction("List_Genders", new { id = 0 });
                    }
                    else
                    {
                        ModelState.AddModelError("GenderTitle", "All Ready Exist!");
                    }
                }
            }
            return View(cRU_GenderMV);
        }
    }
}