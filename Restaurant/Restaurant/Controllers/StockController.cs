﻿using Dblayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Restaurant.Models;
using System.Data;
using System.Data.Entity;
using System.Net;


namespace Restaurant.Controllers
{
    public class StockController : Controller
    {   
       Main_Restaurant_DbEntities db = new Main_Restaurant_DbEntities();
        // GET: Stock

        //Stock Ctagory
        public ActionResult StockitemCategory(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }
            
            var Stockcategories = new CreateRetrieveUpdate_StockItemCategoryMV(id);
            return View(Stockcategories);
        }

        [HttpPost]
        public ActionResult StockitemCategory(CreateRetrieveUpdate_StockItemCategoryMV stockcategory)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }


            if (ModelState.IsValid)
            {
                if (stockcategory.StockitemCategoryID == 0)
                {
                    var checkexist = db.StockitemCategoryTables.Where(s => s.StockitemCategory== stockcategory.StockitemCategory).FirstOrDefault();
        
                    if (checkexist == null)
                    {
                        var newcategory = new StockitemCategoryTable();
                        newcategory.StockitemCategory = stockcategory.StockitemCategory;
                        
                        db.StockitemCategoryTables.Add(newcategory);
                        db.SaveChanges();
                        return RedirectToAction("StockitemCategory", new { id = 0 });

                        
                    }
                    else
                    {
                        ModelState.AddModelError("StockitemCategory", "All Ready Exist!");
                    }
                }
                else
                {
                    var checkexist = db.StockitemCategoryTables.Where(s => s.StockitemCategory == stockcategory.StockitemCategory && s.StockitemCategoryID != stockcategory.StockitemCategoryID).FirstOrDefault();

                    if (checkexist == null)
                    {
                        var editcategory = db.StockitemCategoryTables.Find(stockcategory.StockitemCategoryID);
                        editcategory.StockitemCategory = stockcategory.StockitemCategory;
                        
                        db.Entry(editcategory).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("StockitemCategory", new { id = 0 });
                    }
                    else
                    {
                        ModelState.AddModelError("StockitemCategory", "All Ready Exist!");
                    }
                }
            }
            return View(stockcategory);
        }

        //Stock Item
        public ActionResult StockItem(int?id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }

            var stockitem = new CreateRetrieveUpdate_StockItemMV(id);
            ViewBag.StockitemCategoryID = new SelectList(db.StockitemCategoryTables.ToList(), "StockitemCategoryID", "StockitemCategory", stockitem.StockitemCategoryID);
            return View(stockitem);
        }

        [HttpPost]
        public ActionResult StockItem(CreateRetrieveUpdate_StockItemMV crufood)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }
            crufood.RegisterDate= DateTime.Now;
            if (ModelState.IsValid)
            {
                
                if (crufood.StockitemID == 0)
                {
                    var check=db.StockItemTables.Where(
                        s=>s.StockitemTitle== crufood.StockitemTitle 
                        && s.StockitemCategoryID==crufood.StockitemCategoryID).FirstOrDefault();
                    if (check == null)
                    {
                        var additem = new StockItemTable();
                        ////

                        additem.StockitemCategoryID = crufood.StockitemCategoryID;
                        additem.itemPhotoPath = @"~/Content/Foodimages/default.png";
                        additem.StockitemTitle = crufood.StockitemTitle;
                        additem.itemSize = crufood.itemSize;
                        additem.UnitPrice = crufood.UnitPrice;
                        additem.RegisterDate = DateTime.Now;
                        db.StockItemTables.Add(additem);
                        db.SaveChanges();
                        if (crufood.PhotoPath != null)
                        {
                            var folder = "~/Content/Foodimages";
                            var photoname = string.Format("{0}.jpg", additem.StockitemID);
                            var response = HelperClass.FileUpload.UploadPhoto(crufood.PhotoPath, folder,photoname);
                            if (response)
                            {
                                var photo = string.Format("{0}/{1}", folder, photoname);
                                additem.itemPhotoPath = photo;
                                db.Entry(additem).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                                return RedirectToAction("StockItem", new { id = 0 });

                    }
                    else
                    {
                        ModelState.AddModelError("StockitemTitle", "Items already registered");
                    }
                }
                else
                {
                    var check = db.StockItemTables.Where(
                        s => s.StockitemTitle == crufood.StockitemTitle 
                        && s.StockitemCategoryID == crufood.StockitemCategoryID
                        && s.StockitemID==crufood.StockitemID).FirstOrDefault();
                    if (check == null)
                    {
                        var edititem = db.StockItemTables.Find(crufood.StockitemID);
                        edititem.StockitemCategoryID=crufood.StockitemCategoryID;
                        edititem.StockitemTitle = crufood.StockitemTitle;
                        edititem.itemSize = crufood.itemSize;
                        edititem.UnitPrice = crufood.UnitPrice;
                        db.Entry(edititem).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        if (crufood.PhotoPath != null)
                        {
                            var folder = "~/Content/Foodimages";
                            var photoname = string.Format("{0}.jpg", edititem.StockitemID);
                            var response = HelperClass.FileUpload.UploadPhoto(crufood.PhotoPath, folder, photoname);
                            if (response)
                            {
                                var photo = string.Format("{0}/{1}", folder, photoname);
                                edititem.itemPhotoPath = photo;
                                db.Entry(edititem).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        return RedirectToAction("StockItem", new { id = 0 });

                    }
                    else
                    {
                        ModelState.AddModelError("StockitemTitle", "Items already registered");
                    }
                }
            }
            ViewBag.StockitemCategoryID = new SelectList(db.StockitemCategoryTables.ToList(), "StockitemCategoryID", "StockitemCategory", crufood.StockitemCategoryID);
            return View(crufood);
        }


        //Delete Item
        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }
            var stockItemTables = db.StockItemTables.Include(s => s.StockitemCategoryTable);
            return View(stockItemTables.ToList());
        }
        public ActionResult Delete(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockItemTable stockItemTable = db.StockItemTables.Find(id);
            if (stockItemTable == null)
            {
                return HttpNotFound();
            }
            return View(stockItemTable);
        }

        // POST: StockItemTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserTypeID"])))
            {
                return RedirectToAction("Index", "Home");
            }
            StockItemTable stockItemTable = db.StockItemTables.Find(id);
            db.StockItemTables.Remove(stockItemTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}