﻿using Dblayer;
using Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Restaurant.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        Main_Restaurant_DbEntities Db = new Main_Restaurant_DbEntities();
        public ActionResult Login()
        {
            var user = new LoginMV();
            return View(user);
        }

        [HttpPost]
        public ActionResult Login(LoginMV loginMV)
        {

            if (ModelState.IsValid)
            {
                var user = Db.UserTables.Where(
                    u => (u.EmailAddress.Trim() == loginMV.UserName 
                    || u.UserName.Trim() == loginMV.UserName.Trim()) 
                    && u.Password.Trim() == loginMV.Password.Trim()).FirstOrDefault();
                if (user != null)
                {
                    
                    Session["UserID"] = user.UserID;
                    Session["UserTypeID"] = user.UserTypeID;
                    Session["UserName"] = user.UserName;
                    return RedirectToAction("Index", "Home");
                    
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Please Enter Correct User Name and Password!");
                }
            }
            Session["UserID"] = string.Empty;
            Session["UserTypeID"] = string.Empty;
            return View(loginMV);
        }
        public ActionResult Logout()
        {
            /*Session["UserID"] = string.Empty;
            Session["UserTypeID"] = string.Empty;*/
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }









        public ActionResult Register()
        {
            var user = new Reg_UserMV();
            ViewBag.GenderID = new SelectList(Db.GenderTables.ToList(), "GenderID", "GenderTitle", "0");
            return View(user);
        }

        [HttpPost]
        public ActionResult Register(Reg_UserMV reg_UserMV)
        {
            ViewBag.GenderID = new SelectList(Db.GenderTables.ToList(), "GenderID", "GenderTitle", reg_UserMV.GenderID);//it make dropdownlist in cshtml

            reg_UserMV.UserTypeID = 2; // Customer User_Type_ID
            reg_UserMV.RegisterationDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                bool isexist = false;
                var checkexist = Db.UserTables.Where(u => u.UserName.ToUpper().Trim() == reg_UserMV.UserName.ToUpper().Trim()).FirstOrDefault();
                if (checkexist != null)
                {
                    isexist = true;
                    ModelState.AddModelError("UserName", "Already Exist!");
                }
                checkexist = Db.UserTables.Where(u => u.EmailAddress.ToUpper().Trim() == reg_UserMV.EmailAddress.ToUpper().Trim()).FirstOrDefault();
                if (checkexist != null)
                {
                    isexist = true;
                    ModelState.AddModelError("EmailAddress", "Already Exist!");
                }
                if (isexist == false)
                {
                    var user = new UserTable();
                    user.UserTypeID = reg_UserMV.UserTypeID;
                    user.UserName = reg_UserMV.UserName;
                    user.Password = reg_UserMV.Password;
                    user.FirstName = reg_UserMV.FirstName;
                    user.LastName = reg_UserMV.LastName;
                    user.ContactNo = reg_UserMV.ContactNo;
                    user.GenderID = reg_UserMV.GenderID;
                    user.EmailAddress = reg_UserMV.EmailAddress;
                    user.RegisterationDate = reg_UserMV.RegisterationDate;
                    
                    Db.UserTables.Add(user);
                    Db.SaveChanges();
                    return RedirectToAction("Login", "User");
                 

                }
            }

            
            return View(reg_UserMV);

        }
    }
}