﻿using Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Restaurant.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        public ActionResult Catagory()
        {
            return View(); 
        }
        public ActionResult Nonveg()
        {
            var nonveg = new NonvegMV();
            return View(nonveg);
        }
        public ActionResult Veg()
        {   
            var veg=new VegMV();
            return View(veg);
        }
        public ActionResult Drink()
        {   
            var drink=new DrinkMV();
            return View (drink);
        }
        public ActionResult Chinese() 
        {
            var chinese = new ChineseMV();
            return View (chinese);
        }
        public ActionResult Thali() 
        {
            var thali = new ThaliMV();
            return View(thali);
        }
    }
}