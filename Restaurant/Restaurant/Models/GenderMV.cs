﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Restaurant.Models
{
    public class GenderMV
    {
        [Display(Name ="Unique Number")]
        public int GenderID { get; set; }

        [Display(Name ="Unique Gender")]
        public string GenderTitle { get; set; }
    }
}