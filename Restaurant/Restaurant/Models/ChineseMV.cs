﻿using Dblayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restaurant.Models
{
    public class ChineseMV
    {
        Main_Restaurant_DbEntities db=new Main_Restaurant_DbEntities();
        public ChineseMV()
        {
            GetFoods();
        }

        public List<StockItemMV> Foods { get; set; }


        public void GetFoods()
        {
            Foods = new List<StockItemMV>();
            foreach (var item in db.StockItemTables.Where(c => c.StockitemCategoryID == 5).ToList())  // 5 is dishes category id in stockitemcatgorytable
            {

                Foods.Add(new StockItemMV()
                {
                    StockitemID = item.StockitemID,
                    StockitemCategory = item.StockitemCategoryTable.StockitemCategory,
                    itemPhotoPath = item.itemPhotoPath,
                    StockitemTitle = item.StockitemTitle,
                    itemSize = item.itemSize,
                    UnitPrice = item.UnitPrice,
                    RegisterDate = item.RegisterDate,
                });
            }
        }
    }
}