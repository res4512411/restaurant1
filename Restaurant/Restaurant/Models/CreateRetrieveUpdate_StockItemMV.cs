﻿using Dblayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Restaurant.Models
{
    public class CreateRetrieveUpdate_StockItemMV
    {   Main_Restaurant_DbEntities Db = new Main_Restaurant_DbEntities();
        public int StockitemID { get; set; }

        [Required(ErrorMessage = "*")]
        public int StockitemCategoryID { get; set; }
        public string itemPhotoPath { get; set; }

        [Required(ErrorMessage ="*")]
        public string StockitemTitle { get; set; }

        [Required(ErrorMessage ="*")]
        public string itemSize { get; set; }

        [Required(ErrorMessage ="*")]
        public double UnitPrice { get; set; }
        public System.DateTime RegisterDate { get; set; }

        [NotMapped]
        [Display(Name = "PHOTO")]
        public HttpPostedFileBase PhotoPath { get; set; }

        public virtual List<StockItemMV> Lists {get; set;}

        

        public void getallitems()
        {
            Lists = new List<StockItemMV>();
            foreach (var item in new Main_Restaurant_DbEntities().StockItemTables.ToList())
            {
                Lists.Add(new StockItemMV()
                {
                    StockitemID = item.StockitemID,
                    StockitemCategory = item.StockitemCategoryTable.StockitemCategory,
                    itemPhotoPath = item.itemPhotoPath,
                    StockitemTitle = item.StockitemTitle,
                    itemSize=item.itemSize,
                    UnitPrice = item.UnitPrice,
                    RegisterDate = item.RegisterDate
                });
            }
        }

        public CreateRetrieveUpdate_StockItemMV()
        {
            getallitems();

        }

        public CreateRetrieveUpdate_StockItemMV(int? id)
        {
            getallitems();

            var edit=Db.StockItemTables.Find(id);
            if(edit!=null)
            {
                StockitemID = edit.StockitemID;
                StockitemCategoryID = edit.StockitemCategoryID;
                itemPhotoPath = edit.itemPhotoPath;
                StockitemTitle = edit.StockitemTitle;
                itemSize = edit.itemSize;
                UnitPrice = edit.UnitPrice;

            }
            else
            {
                StockitemID = 0;
                StockitemCategoryID = 0;
                itemPhotoPath =string.Empty;
                StockitemTitle = string.Empty;
                itemSize=string.Empty;
                UnitPrice = 0;
                RegisterDate = DateTime.Now;
            }



        }

    }
}