﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Restaurant.Models
{
    public class StockItemMV
    {
        public int StockitemID { get; set; }
        public string StockitemCategory { get; set; }
        public string itemPhotoPath { get; set; }
        public string StockitemTitle { get; set; }
        public string itemSize { get; set; }
        public double UnitPrice { get; set; }
        public System.DateTime RegisterDate { get; set; }




    }
}