﻿using Dblayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Restaurant.Models
{
    public class CreateRetrieveUpdate_StockItemCategoryMV
    {
        public int StockitemCategoryID { get; set; }

        [Display(Name = "CATEGORIES")] 
        [Required(ErrorMessage = "Required !")]
        public string StockitemCategory { get; set; }
        public List<StockitemCategoryMV> Lists { get; set; }


        public CreateRetrieveUpdate_StockItemCategoryMV()
        {
            Lists = new List<StockitemCategoryMV>();
            foreach (var item in new Main_Restaurant_DbEntities().StockitemCategoryTables.ToList())
            {
                Lists.Add(new StockitemCategoryMV()
                {
                    StockitemCategoryID = item.StockitemCategoryID,
                    StockitemCategory = item.StockitemCategory,
                 
                });
            }
        }

        public CreateRetrieveUpdate_StockItemCategoryMV(int? id)
        {
            Lists = new List<StockitemCategoryMV>();
            foreach (var item in new Main_Restaurant_DbEntities().StockitemCategoryTables.ToList())
            {
                Lists.Add(new StockitemCategoryMV()
                {
                    StockitemCategoryID = item.StockitemCategoryID,
                    StockitemCategory = item.StockitemCategory, 
                });
            }


            var checkexist = new Main_Restaurant_DbEntities().StockitemCategoryTables.Where(s => s.StockitemCategoryID == id).FirstOrDefault();
            if (checkexist != null)
            {
                StockitemCategoryID = checkexist.StockitemCategoryID;
                StockitemCategory = checkexist.StockitemCategory;
                
                }
            else
            {
                StockitemCategoryID = 0;
                StockitemCategory = string.Empty;
                

            }
        }


    }
}