﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restaurant.Models
{
    public class StockitemCategoryMV
    {
        public int StockitemCategoryID { get; set; }
        public string StockitemCategory { get; set; }
        
    }
}